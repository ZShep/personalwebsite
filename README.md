## Personal Website

### Local build

* Get all the plugins going using `bundle install`
* Test locally using `bundle exec jekyll serve`

### Configuration

* Pages in the side tab are in the `/tabs/` folder
  ** They are added to the main page through `assets/css/main.scss`
  ** They also require a custom layout

### Localization

To add another language:

* Create a new file in `_data/owner/`

### License

This project is forked by the "Mr Green" Jekyll theme, which can be found at https://www.MrGreensWorkshop.com

Other tools in use:
  - FontAwesome
  - Jekyll