---
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_beat-best-wodka
title: How to Be(at) the Best at Wodka

# post specific
# if not specified, .name will be used from _data/owner/[language].yml
author: Zach
# multiple category is not supported
category: misc
# multiple tag entries are possible
tags: [wodka]
# thumbnail image for post
img: "../img/posts/wodka-best.png"
# disable comments on this page
#comments_disable: true

# publish date
date: 2022-12-29

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-02-10 08:11:06 +0900
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
#published: false
---

It's a custom in the Wodka community that whenever a player reaches #1 on the leaderboard, they write a blog post describing some of their strategy to other players. As the current champion, it's time for me to step up and perform my duties. And what better subject than advice for beating one specific other player?

> note "About Wodka"
> [Wodka](https://kangaroo.games/games/wodka){:target="_blank"} is a trick-taking card game that was designed by friends as part of a boardgame design group in the early 2010s. It is a spiritual successor to Tichu, drastically simplifying the point scoring while similarly complicating the in-round play. Since then, it has been implemented, re-implemented, renamed, re-renamed, refined, printed, and re-printed. For me, it is one of the testaments of how small communities can be the most vibrant and exciting to be in.
>
> This article talks about some of the technical aspects of gameplay, but it doesn't cover the basics!

## Introduction

For some time in the Wodka community, there has been one certain player with a nasty habit of outperforming all others.  This player is very good at counting cards -- a particular challenge in a game in which cards can be copied, passed and their values changed over the course of a round. This affords them the option to play high-risk strategies, such as placing Wodkas late into a round, relying on knowing the remaining cards in play and working around them.

This style of passive play proved particularly dominant with the addition of the Green suit, which has multiple effects which help inform people about the cards in play and manipulate their hands to match. Finally, the addition of the Green Zero card adds extra rewards to players who can make the gamble. This lead to a strategy I have affectionately named "Cold War", where one or more players are extremely passive throughout the entire round, perhaps waiting for others to go out before starting their play for the round. This strategy is much more effective in Wodka (as opposed to, say, Tichu), because the payoff of effects like the Red 0 contribute almost as much to your team's points as going out.

Although this strategy was developed as a consequence of the addition of the Green suit, it is applicable to other rulesets, and for a brief time it appeared that the strongest player had become only more dominant because of it. How do you beat a player who knows what you are holding, and will wait until you are out of strong sets before playing an unbeatable series of tricks?

In this article I will share some of the ideas I have developed to beat such a player, and how you can use them too! The core of my advice is:

- Pass cards in ways to disrupt complex sets and minimise the risk of bombs, rather than transferring low-value cards
- Understand if you should be playing a round aggressively to force action from passive players, or if you can play passively yourself
- Track the high-impact specials and make sure you have plans to mitigate them being used against you

## Passing

The goal of the passing phase should be:

- Maximise the combined strengths of you and your partners' hands
- Minimise the improvements to your enemies' hands

It turns out that this is not as simple as one would think! Here are two common mistakes I see:

- Passing very low single cards (especially 2s) to your enemies

There are many situations where receiving a 2 won't make an opponent's hand much worse. Suppose in the passing phase you are holding a 2/3/3 as well as your other cards. Given you are holding only one 2, it's likely that at least one enemy is holding a 2. It's *not* likely that they're planning on using this 2 for a straight (3s are unlikely to be passed, and there are only 2 more in circulation). So receiving a second 2 transforms their low single card into a low pair -- if anything, you've made their hand slightly stronger! In this case, holding your 2 has more upside potential: receiving a second gives you a nice consecutive pairs, and receiving other low cards could build into a straight.

- Passing extremely high cards (especially As) to your teammate, especially if they have Grand Wodka'd

Moving an Ace from one teammate to another doesn't increase the combined value of your hands -- either way, it (more or less) guarantees one trick for your team. However, other cards might allow your team to win more tricks than it usually would. Passing a single K (when you have no others) to your ally might give them a pair that probably is strong enough to win a trick. High-value specials, such as R3, R7 and RJ, might fill out their hand much better than yours.

Here are some strategies I use:

- Splitting pairs (or triples) amongst enemies ensures that they will receive minimal value
    - Suppose you are holding 2/2/3/4/5/5/5/6. One of your 5s is already going to be used in a straight. It's unlikely that one given enemy is holding the last 5 -- and even if they passed it to you, the 5-bomb wouldn't make your hand significantly stronger (it would likely be used as a triple). In this case, I would give one 5 to each enemy, where it is very likely to clog their hand as a single card. The 2 is not great as a single card -- but its value in enemies' hands could be much larger
- Consider what signals you are giving to your partner when passing to them
    - Cards like the G10, GQ and B7 are cards which encourage active play to your partner. They signal that you are strongly considering placing a Wodka
- Consider the risks of gambling on receiving cards to complete a combo
    - If you are relying on receiving specific cards (e.g. holding 2/3/4/6 and hoping for a 5), consider what other options you have, and whether it is worth the risk. Depending on the current point situation and the remaining cards in your hand, it might be better to give up the potential set and instead focus on building out a core of moderately effective sets.
- Certain specials are much stronger at certain positions around the table, most notably if a player has placed a Grand Wodka
    - The G10 is usually stronger on the player just before the GW player. This card doesn't win tricks -- it just stops another player from winning them. Placing it before the GW means that other players have a chance to interfere with the trick before you have to commit to playing it

## Know Your Role

I have never played Magic: The Gathering seriously. However, one article that always sticks in my mind when I think of it is named [Who's the Beatdown?](https://articles.starcitygames.com/articles/whos-the-beatdown/){:target="_blank"}. The core idea is that going into a match, a player should understand what role they will play -- should they be trying to win as quickly as possible, or to drag the game out? This is a decision that is dependent on the decks that *both* players have brought. Even if a player has an aggressive deck, they might be playing against an opponent who has an even more aggressive one.

Clearly, Wodka is different in that players do not bring their own decks, but I do think that a similar idea applies. Given a player's hand at the start of the round, they might have different goals for how they play it out. There are some simple examples of this:

- If your partner has placed a (Grand) Wodka, you (probably) want to play cards in a way that will help them go out
- If your team is on 13 points and the other team is on 9 or fewer points, you want to play to sabotage the enemy player with the weakest hand

However, I think that there are a few more considerations that can be made. A core concept is to evaluate the relative strength of your hand -- do you think that you have confidently the best hand? Do you think that you have easily the weakest? Even in the most extreme cases, where you think you are guaranteed to go out (or not), you can alter your playstyle. Secondly, an important consideration is where you sit, especially when a player has placed a Wodka. 

Again, there are some simple examples:

- If, in the passing phase, you believe you have a terrible hand, you should avoid passing away 0s. Then at least your enemies can't benefit from them
    - If, on the other hand, you believe you have a mid-valued hand, you can pressure your enemies into playing the 0s if they believe it is the only way to go out themselves
- If you are playing immediately before a player who has placed a Wodka, you choose what cards they "see" -- playing high cards early (even if it isn't best for you individually to go out) puts more pressure on the player with a Wodka
    - This can include playing paired cards as singles in order to increase the pressure on the player with a Wodka
- If you are playing immediately after a player who has placed a Wodka, you can restrict their partner's ability to play support cards
    - Covering low single cards with 6s or higher prevents the strongest Red support cards

Throughout the course of a round, you should make a conscious effort to see how the other players position themselves with respect to these ideas. Did you pass a 0 to a player and then they lead a single 2?

## Playing to the Suits

It's important to play around the suits that are in play in any game. Although you don't necessarily have to count effects, you should have a plan for how you'll deal with each of them in a round:

- When a Wish effect (e.g. the Red 8) is in play, make sure you have "spare" cards of the suits matching your high-value cards. For example, if you have the Blue Ace, you should hold a low-value Blue card until the Ace is used.
  - Similarly, it can be wise to hold 2s until after the Red 3 is played -- this minimises the impact of receiving them as part of the pass. If an enemy passes you a 4 or higher, at least you can play it on the 3!
- If Green is active, you should look for which card you intend to promote. This is one reason why the Green 9 is so particularly strong when your partner has placed a Wodka -- you can also use it to support them by promoting to a 10 and defusing an enemy's Ace.
