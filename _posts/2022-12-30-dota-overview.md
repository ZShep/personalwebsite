---
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_dota-overview
title: An Overview of Dota 2 Modding

# post specific
# if not specified, .name will be used from _data/owner/[language].yml
author: Zach
# multiple category is not supported
category: programming
# multiple tag entries are possible
tags: [programming, dota]
# thumbnail image for post
img: "../img/posts/swat-reborn.png"
# disable comments on this page
#comments_disable: true

# publish date
date: 2022-12-30

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-02-10 08:11:06 +0900
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
#published: false
---

DotA 2 (short for Defense of the Ancients) is a game by Valve,
which contains a built-in "Arcade" in which people can create
and publish independently made modifications. This Arcade is
inspired by Warcraft 3 - a game released by Blizzard Entertainment
which in turn contained a custom game system that spawned the
original DotA.

Although the DotA 2 engine is far from perfect, it is extremely
powerful. The majority of code is written in LUA (the C++ code
upon which the engine is built is obscured from modders), with
user interface components written in XML/CSS/Javascript.

The majority of my work in the DotA 2 engine was
contributing towards the 
[SWAT: Reborn](https://steamcommunity.com/sharedfiles/filedetails/?id=725282388){:target="_blank"}
arcade game.
SWAT: Aftermath was a popular modification for Warcraft 3
almost 10 years before, and with the release of a new
game with support for modders, the community came
together again. The game is popular because of the
level of teamwork needed, and its difficulty, especially
at the higher levels. The hardest difficulty level -
Extinction - has been beaten only a few dozen times,
requiring the best players from around the world to
gather together.

I joined the game's team shortly after its
"1.0" release, working on it for 3 years. During this time, I:

- Added a new map offering a different twist to the
objective mode
- Fully overhauled the user interface
- Helped with significant balance changes across the board

As well as my work on SWAT: Reborn, I contributed frequently
to the rest of the DotA modding community. I have created
open-source libraries for some of the more advanced 
features we built for the SWAT gamemode, which see use
in several mods by other developers.