---
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_mac-and-cheese
title: Baked Mac and Cheese

# post specific
# if not specified, .name will be used from _data/owner/[language].yml
author: Zach
# multiple category is not supported
category: cooking
# multiple tag entries are possible
tags: [cooking]
# thumbnail image for post
img: "../img/posts/mac-and-cheese.jpg"
# disable comments on this page
#comments_disable: true

# publish date
date: 2022-12-31

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-02-10 08:11:06 +0900
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
#published: false
---

## Overview

This is a relatively fancy baked Mac and Cheese recipe. Reasonably speaking it makes about 6 (unhealthy) portions when served alone, but it's probably best to mix it with some roast vegetables or a salad to take the edge off a little bit.

## Ingredients

* 100g Panko
* 50g Parmesan
* 70g+70g Butter
* 250g Cheddar
* 250g Gruyere
* 500g Pasta
* 50g White Flour
* 700ml Whole Milk

## Plan

```mermaid!
%%{
  init: {
    'theme': 'base',
    'themeVariables': {
      'darkMode': 'true',
      'background': '#2a2a2a',
      'primaryColor': '#06105d',
      'primaryTextColor': '#f7f7ff',
      'lineColor': '#848fa5',
      'secondaryColor': '#577399',
      'secondaryTextColor': '#bdd5ea',
      'tertiaryColor': '#577399',
      'tertiaryTextColor': '#577399',
      'fontFamily': 'verdana',
      'fontSize': '48px'
    }
  }
}%%
gantt
    dateFormat  HH-mm
    axisFormat %H-%M

    section Saucepan
    Cook Pasta           :pasta, 00-00, 10m
    Make Roux            :roux, after pasta  , 5m
    Add Cheese        :cheese, after roux , 5m

	section Baking Dish
	Oven                 :oven, 00-20, 25m

    section Mise
    Mix Topping          :00-00, 5m

```

## Description


Cook the pasta until al dente and set aside. In the same saucepan make a roux:

* Melt half the butter on medium heat
* Add flour and whisk until the colour barely changes (about 1 minute)
* Slowly stream in the milk whilst constantly whisking
* Turn the heat down and wait for the milk to start giving off a little steam

Add the majority of the Cheddar and Gruyere and stir occasionally until melted in. Re-add the pasta, then pour into a baking tray, with the remaining cheese added in the middle.

Mix together the breadcrumbs, grated parmesan, the other half of the butter, and any nice herbs on hand. Cover the top of the baking tray, then cook for \~25 minutes at 200 degrees.
