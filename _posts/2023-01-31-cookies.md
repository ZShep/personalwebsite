---
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_cookies
title: Cookies
# post specific
# if not specified, .name will be used from _data/owner/[language].yml
author: Zach
# multiple category is not supported
category: cooking
# multiple tag entries are possible
tags: [cooking]
# thumbnail image for post
img: "../img/posts/cookies.png"
# disable comments on this page
#comments_disable: true

# publish date
date: 2023-01-31

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-02-10 08:11:06 +0900
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
#published: false
---

## Overview

This is my go-to recipe for an unhealthy dessert. It makes large cookies, slightly crispy on the outside but doughy in the middle. They're at their best a couple of hours after baking, when the cookie dough has set but the chocolate on the inside is not fully solid yet.

The recipe is strongly influenced by that of [Joshua Weismann](https://www.joshuaweissman.com/post/levian-chocolate-chip-cookies){:target="_blank"}. I don't really care for nuts in cookies, and I found that the size of the cookies that came out was far too large. It's still the case that people commonly take half or less of a cookie despite the reduced size.


## Ingredients

### Dry Mix  
230g cake flour  
270g multi-purpose flour  
5g salt  
8g cornstarch  
6g baking soda

### Wet Mix  
280g butter  
285g brown sugar  
115g white sugar  
2 eggs  
3 egg yolks

### Fillings  
450g of:  
Dark Chocolate 60-70% cacao  
White chocolate  
Milk chocolate  
Walnuts

## Description 

Makes about 20 cookies

- Mix together the dry mix in one bowl.  
- Roughly chop the fllings  
- Melt the butter, let it cool a little, then whisk in the sugar. Add the eggs and egg yolks one at a time, whilst whisking constantly  
- Add the dry mixture and fold using a spatula until integrated  
- Fold in the fillings  
- Refrigerate the mixture for 2-8 hours  
- Scoop the cookie dough into balls (usually about 95g each), pack and refrigerate  
- Bake from cold, 220 degrees celcius for 10-13 minutes. The cookies should look like they are just barely beginning to burn on the top.

## Variations

### Chocolate Cookie Dough

For a chocolate cookie dough, use the following changes to ingredients:

100g plain flour --> 100g cocoa
6g baking soda --> 9g baking soda
280g butter --> 300g butter

Cocoa is a little drier than plain flour, and the flavour needs to be offset with a bit more baking soda. It's nice to use more white chocolate to maintain interesting colours in the dough! You can also gently knead together the regular cookie dough with chocolate cookie dough to make a nice marbled pattern.

It's harder to tell when chocolate cookies are done because there's less of a colour change when looking into the oven. They also take marginally longer to cook. You'll have to rely on good instincts!

### Coarse Salt

Swapping some of the salt from table salt to a coarse salt means that you can taste the salt more strongly in specific bites. This is good or bad depending on your preference.