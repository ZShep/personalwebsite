---
title: Beacon Hill
show_sidebar: True
layout: default
---

In 2015 I spent a year working at the Beacon Hill
Academy in Seongnam, Korea. Academies (학원, or
"Hagwon") are private institutions for students to learn
extra skills outside of standard schooling.

I taught students Science and Computer Programming during
my time there. The make-up of the class varied - from
small classes of second grade students, where the focus
was more on having fun and introducing some new ideas, to
somewhat intensive pre-exam study sessions with
final year highschool students.

A big advantage of this particular Hagwon was the freedom
and trust given to me by the administration. Despite being
in Korea, most of the students had a very strong grasp of
English, which meant it was never an obstacle. Favourite
classes included teaching a student LaTeX in order to
convince them to create some Chemistry revision notes,
teaching students to SSH into individual nodes in a
microcomputer cluster, and recreating the Elephant's
Toothpaste experiment.