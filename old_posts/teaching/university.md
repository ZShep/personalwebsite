---
title: University Courses
show_sidebar: True
layout: default
---

From 2016-2020 I helped in teaching courses at the
University of Luxembourg. I contributed to the following
courses:

- Introduction to IT Security
- Theoretical Computer Science 2
- Security Protocols
- Network Security

The first two courses were taught at the Bachelor's level,
the second two at the Master's level.

Along with the standard fare of PhD students - producing
examples and exercise sheets, writing and grading exams,
I also lead multiple classes across these courses.

A particular favourite was the Introduction to IT
Security course, which looked at different topics across
the broad domain of security. Each week I prepared a
20-30 minute example of the relevance of the topics
discussed in class to the field. This included
visualisations of how cryptography works, examples of
generative adversarial models in machine learning, and
how to use a password cracker.

During my time at Imperial College London for my
Bachelor's and Master's degrees, I also made effort to
work with to my fellow students. I helped produce a set of 
LaTeXed notes for the excellent Group Representation Theory
course taught by Ed Segal. The notes are available
[here](/files/pdf/group-representation-theory.pdf).