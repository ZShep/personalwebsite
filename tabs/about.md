---
layout: about
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_about

# image for page specific usage
img: ":about.jpg"
# publish date (used for seo)
# if not specified, site.time will be used.
#date: 2022-03-03 12:32:00 +0000

# for override items in _data/lang/[language].yml
#title: My title
#button_name: "My button"
# for override side_and_top_nav_buttons in _data/conf/main.yml
#icon: "fa fa-bath"

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-03-03 12:32:00 +0000
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
#published: false
---

## About This Website

### Q&A

- **What's the point of this website?**

I think the most interesting deep dives I've ever had on the internet are from reading through the personal blogs of certain people. 
Whether they were my professors, friends, or just random finds from elsewhere, there's something profound about seeing what someone has chosen to put online in their own corner of the web.
I hope that in some way I can help continue this tradition with my own.

- **Aren't the sections a bit... unrelated?**

Of course, part of having a website is to build a personal brand, and I would love to portray myself as a die-hard enthusiast of whatever my job currently is.
But I'd rather share the things that are relevant to me at whatever point I'm at in my life.

- **I have comments or questions**

That's nice.

### Technology & Credits

I used several repositories as part of this website:

- [Jekyll](https://jekyllrb.com/){:target="_blank"}, a static website generator using Ruby and Markdown files. Plugins:
    - [Premonition](https://github.com/lazee/premonition){:target="_blank"} -- Used for admonition blocks. I have my own custom version because I had some text formatting errors
    - [Spaceship](https://github.com/jeffreytse/jekyll-spaceship){:target="_blank"} -- Used for diagrams using e.g. Mermaid and galleries on pages, but it supports a wide range of features
- [Mr Green Jekyll Theme](https://github.com/MrGreensWorkshop/MrGreen-JekyllTheme){:target="_blank"} is used as a baseline for the layout, styles and content generation
- [FontAwesome](https://fontawesome.com){:target="_blank"} provides icons
- [The GitLab repository for this website](https://gitlab.com/ZSmith/zsmith.gitlab.io){:target="_blank"} is available if you want to see the inside of the sausage!