---
layout: programming
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_programming

# publish date (used for seo)
# if not specified, site.time will be used.
#date: 2022-03-03 12:32:00 +0000

# for override items in _data/lang/[language].yml
#title: My title
#button_name: "My button"
# for override side_and_top_nav_buttons in _data/conf/main.yml
#icon: "fa fa-bath"

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-03-03 12:32:00 +0000
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
#published: false


# you can always move this content to _data/content/ folder
# just create new file at _data/content/links/[language].yml and move content below.
###########################################################
#                Links Page Data
###########################################################
page_data:
  main:
    header: "Programming Projects"
    info: "I think hobby programming is one of the best ways to keep myself sharp, with projects that truly interest me. Here's a curated look at some of the projects that I'm particularly proud of."

  category:
    - title: "Project M"
      type: id_projectm
      color: "#9b9ece"
      description: "Project M was a modification for the Nintendo Wii game Super Smash Brothers: Brawl. We wrote custom code in PowerPC Assembler and injected it into the source code of the game (and later also created custom content files, too). It was my first real foray into programming, and a real trial by fire. Seeing the community evolve from the very beginning to the results that came out was a true blessing."
    - title: "Dota 2 Modding"
      type: id_swat
      color: "#912f56"
      description: "The Dota 2 engine supported the creation of custom modifications with an extensive Lua interface and near-full access to the user interface with an XML/CSS/JS stack. I worked with another developer to help create a remake of one of my favourite games during my teenage years -- SWAT: Reborn, a full overhaul co-operative team game about completing objectives in an organised team against the hordes of a Zombie apocalypse."

  list:
    -
    # project m 
    - type: id_projectm
      title: "Project M Homepage"
      url: "https://projectmgame.com/"
      info: "Homepage of the Project M Super Smash Brothers mod (now retired)."
      newTab: true
    - type: id_projectm
      title: "Reminiscing on Project M"
      url: "../posts/2022-12-01-projectm-backstory"
      info: "A brief description of some of my contributions to Brawl+ (and eventually Project M)"

    # swat
    - type: id_swat
      title: |
        SWAT: Reborn Steam Workshop Page
      url: "https://steamcommunity.com/sharedfiles/filedetails/?id=725282388"
      info: "Steam workshop page for SWAT: Reborn"
      newTab: true
    - type: id_swat
      title: "SWAT: Reborn Intro"
      url: "../posts/2022-12-30-dota-overview"
      info: "An overview of modding in Dota 2 and how this resulted in the SWAT: Reborn game"
    - type: id_swat
      title: |
        Dota 2 Modding Libraries Repository
      url: "https://gitlab.com/dota2-custom-games/modding-resources/dota2-modding-libraries"
      info: "Gitlab repository containing some of the libraries I developed for Dota 2 and shared with the community"
      newTab: true
      

---
