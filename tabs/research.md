---
layout: research
# multilingual page pair id, this must pair with translations of this page. (This name must be unique)
lng_pair: id_research

# publish date (used for seo)
# if not specified, site.time will be used.
#date: 2022-03-03 12:32:00 +0000

# for override items in _data/lang/[language].yml
#title: My title
#button_name: "My button"
# for override side_and_top_nav_buttons in _data/conf/main.yml
#icon: "fa fa-bath"

# seo
# if not specified, date will be used.
#meta_modify_date: 2022-03-03 12:32:00 +0000
# check the meta_common_description in _data/owner/[language].yml
#meta_description: ""

# optional
# if you enabled image_viewer_posts you don't need to enable this. This is only if image_viewer_posts = false
#image_viewer_on: true
# if you enabled image_lazy_loader_posts you don't need to enable this. This is only if image_lazy_loader_posts = false
#image_lazy_loader_on: true
# exclude from on site search
#on_site_search_exclude: true
# exclude from search engines
#search_engine_exclude: true
# to disable this page, simply set published: false or delete this file
#published: false


# you can always move this content to _data/content/ folder
# just create new file at _data/content/links/[language].yml and move content below.
###########################################################
#                Links Page Data
###########################################################
page_data:
  main:
    header: "Research"
    info: "Before entering industry, I completed a PhD in theoretical computer science, with applications in cybersecurity. My main area of research was in reasoning about the security of communication protocols (e.g. TLS, RFID payment systems) by embedding them within mathematical models."

  # To change order of the Categories, simply change order. (you don't need to change list order.)
  category:
    - title: "Bibliography"
      type: id_bibliography
      color: "#9b9ece"
      description: I am listed as a (co-)author on the following papers. Links are to the associated publisher page (e.g. IEEE).

  list:
    -
    # messageforwarding
    - type: id_bibliography
      title: "Modelling Agent-Skipping Attacks in Message Forwarding Protocols"
      newTab: true
      year: 2022
      conference: arXiv
      url: "https://arxiv.org/abs/2201.08686"
      info: "Collaboration with Hugo Jonker, Sjouke Mauw and Hyunwoo Lee, based on the final work in my PhD thesis. A major class of multi-party communication protocols are those in which a message is passed up a chain of participants, rather than them communicating mutually between each other. In several scenarios -- including TLS and the Bitcoin Lightning network -- adversarial agents may benefit from artificially removing participants from the chain by sending messages out-of-band. We develop a framework for describing these protocols and appropriate security goals, including modelling classic attacks on the Lightning Network and multiparty TLS protocols"

    # lorawan
    - type: id_bibliography
      title: "Extensive Security Verification of the LoRaWAN Key-Establishment: Insecurities & Patches"
      newTab: true
      year: 2020
      conference: EuroS&P
      url: "https://ieeexplore.ieee.org/abstract/document/9230413"
      info: "Supporting work with Steve Wesemeyer, Ioana Boureanu and Helen Treharne at the University of Surrey. The LoRaWAN protocol is used in a variety of IoT devices, typically to connect to a home server. This protocol involves an establishment phase in which a relay server brokers the channel between the endpoint and application server, under an implicit trust model. A high-fidelity model was created and analysed with Tamarin, and vulnerabilities identified (and confirmed by the security team at LoRa)"

    # collusion
    - type: id_bibliography
      title: "Post-Collusion Security and Distance Bounding"
      newTab: true
      year: 2019
      conference: CCS
      url: "https://dl.acm.org/doi/10.1145/3319535.3345651"
      info: "Followup to previous work with Sjouke Mauw, Jorge Toro-Pozo and Rolando Trujillo-Rasua. We developed a framework for analysing collusion in security protocols -- agents temporarily deviating from their specification in order to break security roles. Informally, a protocol is deemed 'collusion-resistant' if it's not possible to temporarily break the protocol; the smallest violation you can make is essentially equivalent to disclosing your secret encryption keys. We use this model to find vulnerabilities in RFID card payment protocols"

    # matls
    - type: id_bibliography
      title: "maTLS: How to Make TLS middlebox-aware?"
      newTab: true
      year: 2019
      conference: NDSS
      url: "https://www.ndss-symposium.org/wp-content/uploads/2019/02/ndss2019_01B-6_Lee_paper.pdf"
      info: "Collaboration with Seoul National University -- Hyunwoo Lee, Junghwan Lim, Gyeongjae Choi, Selin Chun, Taejoong Chung and Ted Taekyoung Kwon. Despite TLS being a 2-party protocol, in real-world deployments it is frequently used to bridge multiple agents together. This results in insecure practices by creators of so-called 'middleboxes'. We propose a scheme for implementing a multi-party version of TLS, with relevant security properties and a discussion of some of the previous proposals"

    # passport
    - type: id_bibliography
      title: "Breaking Unlinkability of the ICAO 9303 Standard for e-Passports Using Bisimilarity"
      newTab: true
      year: 2019
      conference: ESORICS
      url: "https://link.springer.com/chapter/10.1007/978-3-030-29959-0_28"
      info: "Collaboration with Ihor Filimonov, Ross Horne and Sjouke Mauw at the University of Luxembourg. A security property known as 'unlinkability', despite being informally understood for some time, possesses several different definitions that are seen as broadly equivalent. We show that a new approach for checking process equivalence, which is faithful to the original definitions of unlinkability, gives surprising results when applied to an RFID protocol used by passports that is traditionally assumed to be secure"

    # dbsec
    - type: id_bibliography
      title: "Distance-Bounding Protocols: Verification without Time and Location"
      newTab: true
      year: 2018
      conference: S&P
      url: "https://ieeexplore.ieee.org/document/8418584"
      info: "Work with Sjouke Mauw, Jorge Toro-Pozo and Rolando Trujillo-Rasua at the University of Luxembourg. Distance Bounding protocols are communication protocols with a unique requirement -- physical proximity between participants, usually measured by ping. Modelling these within a mathematical framework which would prefer to abstract details such as location is thus a challenge. We develop such a framework -- and use it to model all of the major protocols in the literature"

    # desynch
    - type: id_bibliography
      title: "Automated Identification of Desynchronisation Attacks on Shared Secrets"
      newTab: true
      year: 2018
      conference: ESORICS
      url: "https://link.springer.com/chapter/10.1007/978-3-319-99073-6_20"
      info: "Work with Sjouke Mauw, Jorge Toro-Pozo and Rolando Trujillo-Rasua at the University of Luxembourg. Rolling key updates are used as a mechanism in certain protocols -- never re-using an encryption key is a simple way to prevent replay attacks, especially in devices with limitated cryptographic capability. However, this leads to new attacks in which the adversary can trick participants into desynchronisation; a state in which they can no longer communicate, because their views of the shared state are no longer compatible. We build a framework for defining these protocols and their security"

      
---
